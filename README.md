# Tooling

In this file we will be discussing the tolls we have recently used and how we can use them as devops

## Trello

Trello is a collaboration tool that organizes your projects into boards

### Trello Functions
 
 - Making Boards
 - Making tickets/cards
 - Assigning members (to cards)
 - Check lists 
 - Labelling

## Slack

Slack is a messaging app for business that connects people to the information they need.

### Slack Functions

 - Creating channels
 - Communicate with team members
 - Send and receive code 
 - Socialise

## Google Classroom 

You can use Classroom in your school to streamline assignments, boost collaboration, and foster communication.

### Google Classroom Functions

 - Submit Assignments/Classwork
 - Stream
 - Share Recordings


## OhMyZsh

Oh My Zsh is a delightful, open source, community-driven framework for managing your Zsh configuration.

### OhMyZsh Functions

 - Allows you to differentiate between files and folders
 - Spelling correction
 - Better theme and plugin support
 - cd Automation
 

## VsCode

Visual Studio Code is a streamlined code editor with support for development operations like debugging, task running, and version control.

### VsCode Functions

 - Refactoring and debug code
 - Syntax highlighting
 - Plugin support for many languages
 - Embedded git



